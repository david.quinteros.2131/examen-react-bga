import React, { useState } from 'react'
import Home from '../components/Home'
import '../styles/main.css'
import { lista } from '../assets/lista'
import Header from '../components/Header'


const Propiedades = () => {

    const [array, setArray] = useState(lista)
    const [ver, setVer] = useState(false);
    const valueToState = (texto) => {
        console.log('texto :>> ', texto);
        aplicarFiltro(texto);
    }

    const aplicarFiltro = (texto) => {
        if(texto == '') {
            setArray(lista);
            setVer(false);
        } else{ 
            const newData = array.filter(d => {
                return d.title.includes(texto);
            });
            console.log('newData :>> ', newData);
            if (newData.length > 0) {
                setArray(newData);
                setVer(false);
            } else {
                setArray(lista);
                setVer(true);
            }
        }
       
    }
    return (
        <div>
            <div >
                <Header valueToState={valueToState}></Header>
            </div>
            <div>
                {ver && (
                    <div  className="alert">
                    <span className="closebtn"
                    onClick = { () => {
                        console.log(false)
                        setVer(false);
                    }}
                       >&times;</span>
                    <strong>Alerta!</strong> No encontramos ninguna coincidencia. Intenta con otra frase.
                </div>
                )}
                
            </div>
            <div className="contenedor">

                <Home title={"Casas en Bolivia"} data={array} />
            </div>

        </div>
    )
}

export default Propiedades
