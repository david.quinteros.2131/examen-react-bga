import React from 'react'
import '../styles/header.css'



const Header = ( {  valueToState } ) => {
    console.log('valueToState :>> ', valueToState);
    let texto = ""
    const handleChange = (e, myCallback) => {
        // console.log('e :>> ', e.target.value);
        texto = e.target.value;

    }

    return ((
        <ul className="topnav">
            <li><a className="active" href="#home">Home</a></li>
            <li><a href="#news">News</a></li>
            <li><a href="#contact">Contact</a></li>
            <li className="right">
                <div className="input" >
                    <form>

                        <input 
                        className="i"
                        // value={texto}
                        name="texto"
                        placeholder="Buscar por titulo..."
                        onChange={handleChange} 
                        type="text" />
                    </form>
                    <div className="div-btn">
                    <a  type="btn" 
                        className="btn" 
                        onClick={() => valueToState(texto)}> buscar </a>

                    </div>

                </div>
            </li>
        </ul>
    ))
}

export default Header